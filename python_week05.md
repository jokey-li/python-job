### 购物车项目  

购物车程序项目要求：  
  1、运行程序后，让用户输入支付宝余额，然后打印我们的商品列表给用户。  
  2、让用户输入商品编号进行商品的购买。  
  3、用户选择商品后，检查用户的余额是否够，若够则直接扣款，不够则提醒用户。  
  4、用户可以随时退出购买，退出时打印用户已购买的商品和支付宝余额。  
```
# Author:jiahao

import random

shopping_list_name = [
    ("Air Jordan 1",1200),
    ("Air Jordan 11",2200),
    ("Nike sb dunk",5000),
    ("yeezy",3000),
    ("anta",1000)
]

aipay = int(input("请输入余额："))
user_shopping_list = []

while True:
    print("-------☆商品☆-------")
    for item in shopping_list_name:
         print(shopping_list_name.index(item),item)

    user_chioce = int(input("请输入商品编号："))
    if user_chioce >= 0 and user_chioce < len(shopping_list_name):
        if aipay >= shopping_list_name[user_chioce][1]:
            user_shopping_list.append(shopping_list_name[user_chioce])
            aipay = aipay - shopping_list_name[user_chioce][1]
            print("已为您添加该商品，您的余额还有%s" %(aipay))
            shopping = input("是否继续购物（请输入是或否）：")
            if shopping == "否":
                for item in user_shopping_list:
                    print("您购买了：%s"%(item[0]),"支付宝余额为：%s" %(aipay))
                break
        else:
            print("您的余额不足，请充值！")
            break

    else:
        print("商品不存在")
```
#### 运行结果
```
请输入余额：5000
-------☆商品☆-------
0 ('Air Jordan 1', 1200)
1 ('Air Jordan 11', 2200)
2 ('Nike sb dunk', 5000)
3 ('yeezy', 3000)
4 ('anta', 1000)
请输入商品编号：2
已为您添加该商品，您的余额还有0
是否继续购物（请输入是或否）：否
您购买了：Nike sb dunk 支付宝余额为：0

进程已结束，退出代码 0
```