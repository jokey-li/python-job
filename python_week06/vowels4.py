vowels = ['a', 'e', 'i', 'o', 'u']
word = input("Provide a word to search for vowels: ")

found = {}

found['a'] = 0
found['e'] = 0
found['i'] = 0
found['o'] = 0
found['u'] = 0

for letter in word:
    if letter in vowels:
        found[letter] += 1

for k, v in sorted(found.items()):
    print(k, 'was found', v, 'time(s)')

# Provide a word to search for vowels: sddddddatfgewrebbrdjdrsaiovwhrebiNQ	SDXKADSM GFSEIRA0UJ GDVFLKSMpoemjryoihrtejmrlmeiu0renmpaom,fposmeoignw4eiuhnoditfmjhpormhpoireg
# a was found 3 time(s)
# e was found 10 time(s)
# i was found 8 time(s)
# o was found 9 time(s)
# u was found 2 time(s)
