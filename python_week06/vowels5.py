vowels = ['a', 'e', 'i', 'o', 'u']
word = input("Provide a word to search for vowels: ")

found = {}

for letter in word:
    if letter in vowels:
        found[letter] += 1

for k, v in sorted(found.items()):
    print(k, 'was found', v, 'time(s)')

# Provide a word to search for vowels: open the door
# Traceback (most recent call last):
#   File "D:/python课程文件/vowels5.py", line 8, in <module>
#     found[letter] += 1
# KeyError: 'o'