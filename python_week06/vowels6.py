vowels = ['a', 'e', 'i', 'o', 'u']
word = input("Provide a word to search for vowels: ")

found = {}

for letter in word:
    if letter in vowels:
        found.setdefault(letter, 0)
        # setdefault() 函数和 get()方法 类似, 如果键不存在于字典中，将会添加键并将值设为默认值。
        found[letter] += 1

for k, v in sorted(found.items()):
    print(k, 'was found', v, 'time(s)')

# Provide a word to search for vowels: i am your friend
# a was found 1 time(s)
# e was found 1 time(s)
# i was found 2 time(s)
# o was found 1 time(s)
# u was found 1 time(s)
