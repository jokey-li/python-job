r = [{'face_token': 'ad1213e50a6c0f81f073293513d85b4b',
   'face_rectangle': {'top': 71, 'left': 492, 'width': 95, 'height': 95},
   'attributes': {
    'age': 20,
    'smile': 0.129,
    'emotion':[ {'anger': 0.009,
     'disgust': 0.002,
     'fear': 0.002,
     'happiness': 0.009,
     'neutral': 99.549,
     'sadness': 0.428,
     'surprise': 0.002}]}},
  {'face_token': 'cbafc595794e9b28b8abad8dc585d588',
   'face_rectangle': {'top': 69, 'left': 377, 'width': 94, 'height': 94},
   'attributes': {
    'age': 20,
    'smile': 0.027,
    'emotion': [{'anger': 0.0,
     'disgust': 0.0,
     'fear': 0.0,
     'happiness': 0.0,
     'neutral': 99.999,
     'sadness': 0.0,
     'surprise': 0.001}]}},
  {'face_token': '5ccc3a996798137756611369acf09b04',
   'face_rectangle': {'top': 88, 'left': 70, 'width': 93, 'height': 93},
   'attributes': {
    'age': 21,
    'smile': 0.009,
    'emotion': [{'anger': 0.003,
     'disgust': 0.003,
     'fear': 0.003,
     'happiness': 0.668,
     'neutral': 99.234,
     'sadness': 0.046,
     'surprise': 0.043}]}},
  {'face_token': '8b95990d7ee863440864e3c370026186',
   'face_rectangle': {'top': 95, 'left': 239, 'width': 87, 'height': 87},
   'attributes': {
    'age':  24,
    'smile': 0.0,
    'emotion': [{'anger': 0.0,
     'disgust': 0.0,
     'fear': 0.005,
     'happiness': 0.0,
     'neutral': 99.98,
     'sadness': 0.0,
     'surprise': 0.014}]}}]

# 创建字典
age = {}
smile = {}
emotion = {}

# 把所有人脸的值添加进字典
count = 0
while count < len(r):
    age["Person{}".format(count + 1)] = r[count]['attributes']['age']
    smile["Person{}".format(count + 1)] = r[count]['attributes']['smile']
    emotion["Person{}".format(count + 1)] = r[count]['attributes']['emotion']
    count += 1
# 打印字典
print("年龄：", age)
print("微笑指数：", smile)
print("情绪指数：", emotion)

# 年龄： {'Person1': 20, 'Person2': 20, 'Person3': 21, 'Person4': 24}
# 微笑指数： {'Person1': 0.129, 'Person2': 0.027, 'Person3': 0.009, 'Person4': 0.0}
# 情绪指数： {'Person1': [{'anger': 0.009, 'disgust': 0.002, 'fear': 0.002, 'happiness': 0.009, 'neutral': 99.549, 'sadness': 0.428, 'surprise': 0.002}], 'Person2': [{'anger': 0.0, 'disgust': 0.0, 'fear': 0.0, 'happiness': 0.0, 'neutral': 99.999, 'sadness': 0.0, 'surprise': 0.001}], 'Person3': [{'anger': 0.003, 'disgust': 0.003, 'fear': 0.003, 'happiness': 0.668, 'neutral': 99.234, 'sadness': 0.046, 'surprise': 0.043}], 'Person4': [{'anger': 0.0, 'disgust': 0.0, 'fear': 0.005, 'happiness': 0.0, 'neutral': 99.98, 'sadness': 0.0, 'surprise': 0.014}]}