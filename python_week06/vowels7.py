vowels = set('aeiou')
word = input("Provide a word to search for vowels: ")
found = vowels.intersection(set(word))
for vowel in found:
    print(vowel)

# Provide a word to search for vowels: thank you very much
# u
# o
# e
# a