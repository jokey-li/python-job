### week_04

##### 1. 尝试用户输入学生一周的课表信息，尝试用占位符、2种format打印学生信息（可设计）

```
print ("请输入你的课程信息")
一 = input("星期一：")
二 = input("星期二：")
三 = input("星期三：")
四 = input("星期四：")
五 = input("星期五：")

info = '''------ 课程表
星期一 : '''+一+'''
星期二 : '''+二+'''
星期三 : '''+三+'''
星期四 : '''+四+'''
星期五 : '''+五+'''

 '''
print(info)

# 2、占位符

info_02 = '''------课程表
星期一:%s
星期二:%s
星期三:%s
星期四:%s
星期五:%s
''' % (一,二,三,四,五)
print(info_02)

# 3、 format

info_03 = '''------课程表
星期一:{}
星期二:{}
星期三:{}
星期四:{}
星期五:{}

'''.format(一,二,三,四,五)
print(info_03)

info_04 = '''------课程表
星期一:{周一}
星期二:{周二}
星期三:{周三}
星期四:{周四}
星期五:{周五}
 
 '''.format(周一=一,周三=三,周二=二,周五=五,周四=四)
print(info_04)
```
```
运行结果：

请输入你的课程信息
星期一：中西方哲学社会科学研究，大学英语（三）
星期二：ai，定向越野，广告心理学，舞蹈鉴赏
星期三：大学英语（三），Python语言
星期四：毛概实践，网站运营与管理，API
星期五：毛概理论，写作训练
------ 课程表
星期一 : 中西方哲学社会科学研究，大学英语（三）
星期二 : ai，定向越野，广告心理学，舞蹈鉴赏
星期三 : 大学英语（三），Python语言
星期四 : 毛概实践，网站运营与管理，API
星期五 : 毛概理论，写作训练

 
------课程表
星期一:中西方哲学社会科学研究，大学英语（三）
星期二:ai，定向越野，广告心理学，舞蹈鉴赏
星期三:大学英语（三），Python语言
星期四:毛概实践，网站运营与管理，API
星期五:毛概理论，写作训练

------课程表
星期一:中西方哲学社会科学研究，大学英语（三）
星期二:ai，定向越野，广告心理学，舞蹈鉴赏
星期三:大学英语（三），Python语言
星期四:毛概实践，网站运营与管理，API
星期五:毛概理论，写作训练


------课程表
星期一:中西方哲学社会科学研究，大学英语（三）
星期二:ai，定向越野，广告心理学，舞蹈鉴赏
星期三:大学英语（三），Python语言
星期四:毛概实践，网站运营与管理，API
星期五:毛概理论，写作训练
 
 

进程已结束，退出代码 0

```

##### 2. 用户输入查询日期和时间，可返回对应的课表信息；用户查询当前时间，可返回当前课表信息

```
week = input("星期")
time = int(input("时间（例：下午一点三十分=1330）："))

if week == "一":
    if 1035 < time < 1200:
        print("中西方哲学社会科学研究")
    elif 1610 < time < 1735:
        print("大学英语（三）")
    else:
        print("你没课！")
elif week == "二":
    if 1250 < time < 1420:
        print("Illustrator软件应用")
    elif 1430 < time < 1555:
        print("定向越野")
    elif 1610 < time < 1735:
        print("广告心理学")
    elif 1845 < time < 2010:
        print("舞蹈鉴赏")
    else:
        print("你没课！")
elif week == "三":
    if 1250 < time < 1420:
        print("大学英语（三）")
    elif 1845 < time < 2055:
        print("Python 语言")
    else:
        print("你没课！")
elif week == "四":
    if 1250 < time < 1420:
        print("毛泽东思想和中国特色社会主义理论体系概论（实践）")
    elif 1430 < time < 1650:
        print("网站运营与管理")
    elif 1845 < time < 2055:
        print("API、机器学习与人工智能")
    else:
        print("你没课！")
elif week == "五":
    if 800 < time < 1025:
        print("毛泽东思想和中国特色社会主义理论体系概论")
    elif 1845 < time < 2010:
        print("写作训练")
    else:
        print("你没课！")
else:
    print("你放假了！！")
```
```
运行结果：

星期一
时间（例：下午一点三十分=1330）：1100
中西方哲学社会科学研究

进程已结束，退出代码 0
```

##### 用户查询当前时间，可返回当前课表信息

```
import time

today = time.strftime("%A")
now = int(time.strftime("%H%M"))

if today == "Monday":
    if 1035 < now < 1200:
        print("中西方哲学社会科学研究")
    elif 1610 < now < 1735:
        print("大学英语（三）")
    else:
        print("你没课！")
elif today == "Tuesday":
    if 1250 < now < 1420:
        print("Illustrator软件应用")
    elif 1430 < now < 1555:
        print("定向越野")
    elif 1610 < now < 1735:
        print("广告心理学")
    elif 1845 < now < 2010:
        print("舞蹈鉴赏")
    else:
        print("你没课！")
elif today == "Wednesday":
    if 1250 < now < 1420:
        print("大学英语（三）")
    elif 1845 < now < 2055:
        print("Python 语言")
    else:
        print("你没课！")
elif today == "Thursday":
    if 1250 < now < 1420:
        print("毛泽东思想和中国特色社会主义理论体系概论（实践）")
    elif 1430 < now < 1650:
        print("网站运营与管理")
    elif 1845 < now < 2055:
        print("API、机器学习与人工智能")
    else:
        print("你没课！")
elif today == "Friday":
    if 800 < now < 1025:
        print("毛泽东思想和中国特色社会主义理论体系概论")
    elif 1845 < time < 2010:
        print("写作训练")
    else:
        print("你没课！！")
else:
    print("放假啦！！！")
```
```
运行结果：

你没课！

进程已结束，退出代码 0
```

##### 列表的使用方法
```
names = ["周铭峰","李嘉浩","橙子基","fl","zhuzhu"]
print(names)

#1、增加 append 加在列表末尾
names.append("肖亮书")
print(names)

# insert 增加到指定位置

names.insert(2,"李婧雯")
print(names)

# 2、删除 pop() (1)如果什么都不填 删除末位 (2)删除指定位置的列表内容
names.pop()
print(names)

names.pop(2)
print(names)

pop_names = names.pop(2) # 查询被删除的内容
print(pop_names)
print(names)

# remove 可以删除指定内容
names.remove( "李嘉浩" )
print(names)

# 3、修改
names[0] = "陈颖"
print(names)

运行结果：
['周铭峰', '李嘉浩', '橙子基', 'fl', 'zhuzhu']
['周铭峰', '李嘉浩', '橙子基', 'fl', 'zhuzhu', '肖亮书']
['周铭峰', '李嘉浩', '李婧雯', '橙子基', 'fl', 'zhuzhu', '肖亮书']
['周铭峰', '李嘉浩', '李婧雯', '橙子基', 'fl', 'zhuzhu']
['周铭峰', '李嘉浩', '橙子基', 'fl', 'zhuzhu']
橙子基
['周铭峰', '李嘉浩', 'fl', 'zhuzhu']
['周铭峰', 'fl', 'zhuzhu']
['陈颖', 'fl', 'zhuzhu']
```

```
列表的切片：

names = ["66","77","88","99","00"]
print(names[1],names[2])

# 切片 仍然是一个列表  回顾 range（1，3）  末位不取
print(names[1:3])

# 如果从列表首位开始，可以省略0
print(names[0:3])
print(names[:3])

# 从末位开始取
print(names[-3:-1])
print(names[-3:])

运行结果：
77 88
['77', '88']
['66', '77', '88']
['66', '77', '88']
['88', '99']
['88', '99', '00']
```

##### 课本练习
```
Vowels：

vowels = ['a', 'e', 'i', 'o', 'u']
word = "Milliways"
for letter in word:
    if letter in vowels:
        print(letter)
结果：
i
i
a

vowels = ['a', 'e', 'i', 'o', 'u']
word = "Milliways"
found = []
for letter in word:
    if letter in vowels:
        if letter not in found:
            found.append(letter)
for vowel in found:
    print(vowel)
结果：
i
a

vowels = ['a', 'e', 'i', 'o', 'u']
word = input("Provide a word to search for vowels: ")
found = []
for letter in word:
    if letter in vowels:
        if letter not in found:
            found.append(letter)
for vowel in found:
    print(vowel)
结果：
Provide a word to search for vowels: elevate
e
a
```
```
Panic：

phrase = "Don't panic!"
plist = list(phrase)
print(phrase)
print(plist)

for i in range(4):
    plist.pop()
plist.pop(0)
plist.remove("'")

plist.extend([plist.pop(), plist.pop()])
plist.insert(2, plist.pop(3))

new_phrase = ''.join(plist)
print(plist)
print(new_phrase)

结果：
Don't panic!
['D', 'o', 'n', "'", 't', ' ', 'p', 'a', 'n', 'i', 'c', '!']
['o', 'n', ' ', 't', 'a', 'p']
on tap

phrase = "Don't panic!"
plist = list(phrase)
print(phrase)
print(plist)

new_phrase = ''.join(plist[1:3])
new_phrase = ''.join([new_phrase, plist[5], plist[4], plist[7], plist[6]])

print(plist)
print(new_phrase)

结果：
Don't panic!
['D', 'o', 'n', "'", 't', ' ', 'p', 'a', 'n', 'i', 'c', '!']
['D', 'o', 'n', "'", 't', ' ', 'p', 'a', 'n', 'i', 'c', '!']
on tap

phrase = "Don't panic!"
plist = list(phrase)
print(phrase)
print(plist)

new_phrase = ''.join(plist[1:3]) + ''.join([plist[5], plist[4], plist[7], plist[6]])

print(plist)
print(new_phrase)

结果：
Don't panic!
['D', 'o', 'n', "'", 't', ' ', 'p', 'a', 'n', 'i', 'c', '!']
['D', 'o', 'n', "'", 't', ' ', 'p', 'a', 'n', 'i', 'c', '!']
on tap
```
```
Marvin

paranoid_android = "Marvin"
letters = list(paranoid_android)
for char in letters:
    print('\t', char)
结果：
     M
	 a
	 r
	 v
	 i
	 n
	 
paranoid_android = "Marvin, the Paranoid Android"
letters = list(paranoid_android)
for char in letters[:6]:
    print('\t', char)
print()
for char in letters[-7:]:
    print('\t'*2, char)
print()
for char in letters[12:20]:
    print('\t'*3, char)
    
结果：

	 M
	 a
	 r
	 v
	 i
	 n

		 A
		 n
		 d
		 r
		 o
		 i
		 d

			 P
			 a
			 r
			 a
			 n
			 o
			 i
			 d

```
##### 思考题

names = [.....]  
* 想找出重名的名字 ------> rename = [..]？  

* 循环遍历每一个元素---->[]  
* 如果遍历到某一个前面出现过，存到一个[]  
* 计算重名个数？ '''

```
names=["浩浩","蓝蓝","峰峰","书书","基基","书书","静静","朱朱","霖霖","蓝蓝","基基"]
rename = {}
for i in names:
  if names.count(i)>1:
    rename[i] = names.count(i)
print (rename)

结果：

{'蓝蓝': 2, '书书': 2, '基基': 2}
```

##### 「列表训练营」
1. **输入一行字符，分别统计出其中英文字母、空格、数字和其它字符的个数。**

```
a = input('请输入一串字符：')
counts = {"文字": 0, "数字": 0, "空格": 0, "其他":0 }

for i in a:
    if i.isalpha():
        counts["文字"] += 1
    elif i.isnumeric():
        counts["数字"] += 1
    elif i.isspace():
        counts["空格"] += 1
    else:
        counts["其他"] += 1

for key, value in counts.items():
    print('%s=' % key, value)
    
结果：

请输入一串字符：啊实打实打算     asfdsafasfsadfsadfsdf      asdasrfasf'sd发‘sa发生发生大事的  
文字= 49
数字= 0
空格= 13
其他= 2
```

2.  **班上投票竞选，将选择票数最高的同学担任班长，请你设计一个投票系统，输入名字即可投票，最后统计大家的得票数，公开投票结果，并宣布谁担任班长。**
```
votes = {"美丽": 0, "漂亮": 0, "可爱": 0}
rounds = 0
while rounds < 5:
    name = input("请输入候选人姓名：")
    if name == "美丽":
        votes["美丽"] += 1
    elif name == "漂亮":
        votes["漂亮"] += 1
    elif name == "好看":
        votes["好看"] += 1
    rounds += 1
print("竞选结果：", end="")
for key, value in votes.items():
    print("{}获得{}票；".format(key, value), end="")
    if value == max(votes.values()):
        winner = key
print("{}成功当选班长。".format(winner))

结果：

请输入候选人姓名：美丽
请输入候选人姓名：美丽
请输入候选人姓名：meil
请输入候选人姓名：美丽
请输入候选人姓名：美丽
竞选结果：美丽获得4票；漂亮获得0票；可爱获得0票；美丽成功当选班长。

```