### python作业
1. 思考题
+ 阅读 Head First Python 引子
+ 描述什么是标准库、开源模块和自定义模块  
**Python标准库**是一组丰富的软件模块，提供了大量预建（而且高质量）的可重用代码。  
**函数+模块=标准库**  
**开源模块**是一种开放型代码，放在官网中供人选择和使用。  
**自定义模块**是其他程序员为了更好地实现某种目标，在实际开发中逐渐衍生出来的函数库或者副产品，然后将其放在互联网上与我们共享。

+ 试回答Python是一种什么样的语言  
**python**是一门动态解释性的强类型定义语言。

2. 练习题  
+ P10-11 模块查询python.org，找到每个模块使用的函数意义，作记录并运行代码
#### sys模块
```
>>> import sys 
>>> sys.platform 
'win32'
```
 `import sys` (导入sys模块)  
 `sys.platform`(访问运行系统）
 
```
>>> print(sys.version)
3.7.0 (default, Jun 28 2018, 08:04:48) [MSC v.1912 64 bit (AMD64)]
```   
`print(sys.version)`(查询python版本)    

#### os模块
```
>>> import os
>>> os.getcwd()
'C:\\Users\\86184'
```
`import os`(导入os模块)
`os.getcwd`(得出代码所在文件夹得名字)

```
>>> os.environ
environ({'ALLUSERSPROFILE': 'C:\\ProgramData', 'APPDATA': 'C:\\Users\\86184\\AppData\\Roaming', 'COMMONPROGRAMFILES': 'C:\\Program Files\\Common Files', 'COMMONPROGRAMFILES(X86)': 'C:\\Program Files (x86)\\Common Files', 'COMMONPROGRAMW6432': 'C:\\Program Files\\Common Files', 'COMPUTERNAME': 'LAPTOP-GOJTJJS6', 'COMSPEC': 'C:\\WINDOWS\\system32\\cmd.exe', 'CONFIGSETROOT': 'C:\\WINDOWS\\ConfigSetRoot', 'DRIVERDATA': 'C:\\Windows\\System32\\Drivers\\DriverData', 'FPS_BROWSER_APP_PROFILE_STRING': 'Internet Explorer', 'FPS_BROWSER_USER_PROFILE_STRING': 'Default', 'HOMEDRIVE': 'C:', 'HOMEPATH': '\\Users\\86184', 'LOCALAPPDATA': 'C:\\Users\\86184\\AppData\\Local', 'LOGONSERVER': '\\\\LAPTOP-GOJTJJS6', 'NUMBER_OF_PROCESSORS': '8', 'ONEDRIVE': 'C:\\Users\\86184\\OneDrive', 'ONEDRIVECONSUMER': 'C:\\Users\\86184\\OneDrive', 'OS': 'Windows_NT', 'PATH': 'C:\\Windows\\system32;C:\\Windows;C:\\Windows\\System32\\Wbem;C:\\Windows\\System32\\WindowsPowerShell\\v1.0\\;C:\\Windows\\System32\\OpenSSH\\;C:\\Program Files (x86)\\NVIDIA Corporation\\PhysX\\Common;C:\\Program Files\\NVIDIA Corporation\\NVIDIA NGX;C:\\Program Files\\NVIDIA Corporation\\NVIDIA NvDLISR;C:\\WINDOWS\\system32;C:\\WINDOWS;C:\\WINDOWS\\System32\\Wbem;C:\\WINDOWS\\System32\\WindowsPowerShell\\v1.0\\;C:\\WINDOWS\\System32\\OpenSSH\\;C:\\Program Files\\Microsoft VS Code\\bin;D:\\Anaconda3;D:\\Anaconda3\\Library\\mingw-w64\\bin;D:\\Anaconda3\\Library\\usr\\bin;D:\\Anaconda3\\Library\\bin;D:\\Anaconda3\\Scripts;D:\\python;D:\\python\\Library\\mingw-w64\\bin;D:\\python\\Library\\usr\\bin;D:\\python\\Library\\bin;D:\\python\\Scripts;C:\\Users\\86184\\AppData\\Local\\Microsoft\\WindowsApps;C:\\Users\\86184\\AppData\\Local\\GitHubDesktop\\bin;C:\\Users\\86184\\AppData\\Local\\Microsoft\\WindowsApps', 'PATHEXT': '.COM;.EXE;.BAT;.CMD;.VBS;.VBE;.JS;.JSE;.WSF;.WSH;.MSC', 'PROCESSOR_ARCHITECTURE': 'AMD64', 'PROCESSOR_IDENTIFIER': 'Intel64 Family 6 Model 158 Stepping 10, GenuineIntel', 'PROCESSOR_LEVEL': '6', 'PROCESSOR_REVISION': '9e0a', 'PROGRAMDATA': 'C:\\ProgramData', 'PROGRAMFILES': 'C:\\Program Files', 'PROGRAMFILES(X86)': 'C:\\Program Files (x86)', 'PROGRAMW6432': 'C:\\Program Files', 'PROMPT': '$P$G', 'PSMODULEPATH': 'C:\\Program Files\\WindowsPowerShell\\Modules;C:\\WINDOWS\\system32\\WindowsPowerShell\\v1.0\\Modules', 'PUBLIC': 'C:\\Users\\Public', 'SESSIONNAME': 'Console', 'SYSTEMDRIVE': 'C:', 'SYSTEMROOT': 'C:\\WINDOWS', 'TEMP': 'C:\\Users\\86184\\AppData\\Local\\Temp', 'TMP': 'C:\\Users\\86184\\AppData\\Local\\Temp', 'USERDOMAIN': 'LAPTOP-GOJTJJS6', 'USERDOMAIN_ROAMINGPROFILE': 'LAPTOP-GOJTJJS6', 'USERNAME': '86184', 'USERPROFILE': 'C:\\Users\\86184', 'WINDIR': 'C:\\WINDOWS'})
>>>os.getenv('HOMEPATH')
'\\Users\\86184'
```
`os.environ`(访问系统全部环境变量)
`os.getenv`(访问指定环境变量)

#### datemine模块
```
>>> import datetime
>>> datetime.date.today()
datetime.date(2020, 9, 22)
```
`import datetime`（导入datetime模块）  
`datetime.date.today()`(查询今日日期)

```
>>> datetime.date.today().day
22
>>> datetime.date.today().month
9
>>> datetime.date.today().year
2020
```
`datetime.date.today().day/month/year`（单独访问今日的日期、月份、年份值）

```
>>> datetime.date.isoformat(datetime.date.today())
'2020-09-22'
```
`datetime.date.isoformat(datetime.date.today())`(今日日期转化为字符串)

```
>>> import time
>>> time.strftime("%H:%M")
'10:41'
```
`import time`（导入time模块）  
`time.strftime("%H:%M")`(调用strftime函数并指定显示时间的方式)
#### HTML模块
```
>>> import html
>>> html.escape("This HTML fragment contains a <script>script</script> tag.")
'This HTML fragment contains a &lt;script&gt;script&lt;/script&gt; tag.'
>>> html.unescape("I &hearts; Python's &lt;standard library&gt;.")
"I ♥ Python's <standard library>."
``` 
`import html`（进入html模块）  
`html.escape("This HTML fragment contains a <script>script</script> tag.")`  
（编码那些可能有麻烦的尖括号）  
`html.unescape("I &hearts; Python's &lt;standard library&gt;.")`  
（恢复一些编码的HTML成原来的形式）

+ 尝试理解if 、else、elif是什么？什么是代码块，并运行P16-18代码  
**if、else、elif** 是解释器对代码的内容的判断，如果表达式为真，则执行“语句块”；如果表达式的值为假，就跳 过“语句块”，继续执行后面的语句。  
**代码块：** 代码块是一组由代码构成的功能“单元”。一个代码块可以单独运行。比如一个函数（function）或一个类（class）定义、一个模块（module）都是代码块。

   **代码**
```
>>> from datetime import datetime
>>> odds = [ 1, 3, 5, 7, 9, 11, 13, 15, 17, 19, 21, 23, 25, 27, 29, 31, 33, 35, 37, 39, 41, 43, 45, 47, 49, 51, 53, 55, 57, 59 ]
>>> right_this_minute = datetime.today().minute
>>> if right_this_minute in odds:
        print("This minute seems a little odd.")
    else:
        print("Not an odd minute.")
```
```
运行结果：

This minute seems a little odd.

```

```
import time
today = time.strftime("%A %p")
if today == 'Saturday':
    print('Party!')
elif today == 'Sunday':
    if condition == 'Headache':
        print('Recover, then rest.')
    else:
        print('Rest.')
else:
    print('Work, work, work.')
```
```
运行结果：  

Work, work, work.
```

+ 尝试理解什么是迭代，并运行P24-25 for循环、range循环代码，互相讨论其用法  

  **Python中的迭代**是指通过重复执行的代码处理相似的数据集的过程，并且本次迭代的处理数据要依赖上一次的结果继续往下做，上一次产生的结果为下一次产生结果的初始状态，如果中途有任何停顿，都不能算是迭代。  
  
  **循环取一个数字列表**

  ```
  >>> for i in[1, 2, 3]:
  ...       print(i)
  ...
  1
  2
  3
  ```
  **循环迭代处理一个字符串**
  ```
  >>> for ch in "Hi!":
  ...        print(ch)
  ...
  H
  i
  !
  ```
  **循环迭代五次**
  ```
  >>> for num in range(5):
  ...       print('Head First Rocks!')
  ...
  Head First Rocks!
  Head First Rocks!
  Head First Rocks!
  Head First Rocks!
  Head First Rocks!
  ```
  **Range试验**
  ```
  >>> range(5)
  range(0, 5)
  >>> list(range(5))
  [0, 1, 2, 3, 4]
  >>> list(range(5,10))
  [5, 6, 7, 8, 9]
  >>> list(range(0,10,2))
  [0, 2, 4, 6, 8]
  >>> list(range(10,0,-2))
  [10, 8, 6, 4, 2]
  >>> list(range(10,0,2))
  []
  >>> list(range(99,0,-1))
  [99, 98, 97, 96, 95, 94, 93, 92, 91, 90, 89, 88, 87, 86, 85, 84, 83, 82, 81, 80, 79, 78, 77, 76, 75, 74, 73, 72, 71, 70, 69, 68, 67, 66, 65, 64, 63, 62, 61, 60, 59, 58, 57, 56, 55, 54, 53, 52, 51, 50, 49, 48, 47, 46, 45, 44, 43, 42, 41, 40, 39, 38, 37, 36, 35, 34, 33, 32, 31, 30, 29, 28, 27, 26, 25, 24, 23, 22, 21, 20, 19, 18, 17, 16, 15, 14, 13, 12, 11, 10, 9, 8, 7, 6, 5, 4, 3, 2, 1]
  ```
+ 学习掌握random、time模块，结合循环、迭代尝试完成P43 （尽量不要翻看P44答案）代码作业

  ```
  >>> import random
  >>> random.randint(1,60)
  19
  >>> random.randint(1,60)
  29
  >>> random.randint(1,60)
  51
  ```
  
 