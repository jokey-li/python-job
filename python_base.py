python_base = {
    "Python 介绍": {
        "Python 历史": "100(70)",
        "编程语言": {
            "分类": "100(70)",
            "Python 优缺点": "100(75)"}
    },
    "IDE": {
        "IDLE": {
            "操作": "100(80)",
            "意义": "100(75)"
        },
        "Jupyter Notebook": {
            "操作": "100(77)",
            "拓展插件": "100(70)"
        },
        "PyCharm": {
            "操作": "100(80)",
            "界面选项": "100(75)"
        }
    },
    "Anaconda": {
        "安装、配置与使用": "100(90)",
        "意义": "100(85)"
    },
    "标准库": {
        "分类": "100(80)",
        "调用": "100(80)"
    },
    "流程控制语句": {
        "条件语句": {
            "if 语句": "100(70)",
            "else 语句": "100(70)",
            "elif 语句": "100(70)"
        },
        "循环语句": {
            "while 循环": "100(70)",
            "for 循环": {
                "迭代字符串、列表": "100(70)",
                "迭代指定的次数、内置函数range()": "100(65)"
            }
        },
        "循环嵌套": "100(70)"
    },
    "变量": {
        "赋值": "100(75)",
        "数据类型": {
            "字符串": {
                "单/双/三引号": "100(80)",
                "转义": "100(70)",
                "占位符与 format() ": "100(70)",
                "拼接": "100(70)",
                "split()": "100(75)",
                "其他字符串内置函数": "100(70)"
            },
        "输出与用户输入": "100(75)"
    },

    "数据结构": {
        "列表": {
            "操作": {
                "增删改": "100(80)",
                "切片": "100(80)"
            },
            "索引": "100(90)"
        },
        "字典": {
            "操作": "100(80)",
            "嵌套": "100(80)"
        },
        "元组": "100(75)",
        "集合": "100(75)",
        "数据访问": "100(75)",
    },
    "函数": {
        "定义": "100(80)",
        "使用": {
            "实参、形参": {
                "位置参数": "100(80)",
                "关键字参数": "100(80)",
                "默认值": "100(80)"
            }
        },
        "返回值": "100(80)",
        "模块": {
            "意义": "100(80)",
            "封装与调用": "100(80)",
            }
        }
    }
}